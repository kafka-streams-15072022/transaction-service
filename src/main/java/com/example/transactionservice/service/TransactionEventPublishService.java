package com.example.transactionservice.service;


import com.example.transactionservice.model.entities.TransactionEntity;
import com.example.transactionservice.model.events.TransactionEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class TransactionEventPublishService {

    @Configuration
    @ConfigurationProperties(prefix = "events")
    public static class Config {

        private String consumerUri;
        private String topicName;

        public String getConsumerUri() {
            return consumerUri;
        }

        public void setConsumerUri(String consumerUri) {
            this.consumerUri = consumerUri;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }
    }

    private final RestTemplate restTemplate;

    private final KafkaTemplate<UUID, Object> kafkaTemplate;
    private final Config config;

    public TransactionEventPublishService(RestTemplate restTemplate, KafkaTemplate<UUID, Object> kafkaTemplate, Config config) {
        this.restTemplate = restTemplate;
        this.kafkaTemplate = kafkaTemplate;
        this.config = config;
    }

    @Transactional
    public void onEntityChange(TransactionEntity entity) {
        final var fromEvent = new TransactionEvent(
                entity.getFrom(),
                entity.getTimestamp(),
                entity.getAmount().negate(),
                entity.getReverted()
        );
        final var toEvent = new TransactionEvent(
                entity.getTo(),
                entity.getTimestamp(),
                entity.getAmount(),
                entity.getReverted()
        );
        if (config.consumerUri != null) {
            sendByHttp(fromEvent);
            sendByHttp(toEvent);
        }
        if (config.topicName != null) {
            sendByKafka(fromEvent);
            sendByKafka(toEvent);
        }
    }

    private void sendByHttp(TransactionEvent event) {
        restTemplate.put(config.consumerUri, event);
    }
    private void sendByKafka(TransactionEvent event) {
        kafkaTemplate.send(config.topicName, event.id, event);
    }
}
