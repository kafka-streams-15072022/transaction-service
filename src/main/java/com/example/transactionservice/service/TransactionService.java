package com.example.transactionservice.service;

import com.example.transactionservice.model.entities.TransactionEntity;
import com.example.transactionservice.storage.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final TransactionEventPublishService eventPublishService;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository, TransactionEventPublishService eventPublishService) {
        this.transactionRepository = transactionRepository;
        this.eventPublishService = eventPublishService;
    }

    @Transactional
    public boolean executeTransaction(UUID id, UUID from, UUID to, BigDecimal amount) {
        if (transactionRepository.existsById(id)) {
            return false;
        }
        final var entity = new TransactionEntity();
        entity.setId(id);
        entity.setFrom(from);
        entity.setTo(to);
        entity.setAmount(amount);
        entity.setTimestamp(ZonedDateTime.now());
        entity.setReverted(false);
        transactionRepository.save(entity);
        eventPublishService.onEntityChange(entity);
        return true;
    }

    @Transactional
    public void revertTransaction(UUID id) {
        final var entityOpt = transactionRepository.findById(id);
        if (entityOpt.isPresent()) {
            final var entity = entityOpt.get();
            entity.setReverted(true);
            transactionRepository.save(entity);
            eventPublishService.onEntityChange(entity);
        }
    }
    @Transactional
    public Optional<TransactionEntity> findTransaction(UUID id) {
        return transactionRepository.findByIdAndReverted(id, false);
    }
}
