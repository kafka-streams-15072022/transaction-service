package com.example.transactionservice.storage;

import com.example.transactionservice.model.entities.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<TransactionEntity, UUID> {

    Optional<TransactionEntity> findByIdAndReverted(UUID id, boolean reverted);
}
