package com.example.transactionservice.controller;

import com.example.transactionservice.model.dto.TransactionDto;
import com.example.transactionservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.UUID;

@RestController
public class TransactionController {

    private final TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PutMapping(path = "transactions/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> executeTransaction(HttpServletRequest request, @PathVariable("id") UUID id, @RequestBody TransactionDto transactionDto) {
        final var created = transactionService.executeTransaction(
                id,
                transactionDto.getFrom(),
                transactionDto.getTo(),
                transactionDto.getAmount()
        );
        return created ? ResponseEntity.created(URI.create(request.getRequestURI())).build() : ResponseEntity.noContent().build();
    }

    @GetMapping(path = "transactions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionDto getTransaction(@PathVariable("id") UUID id) {
        return transactionService.findTransaction(id).map(entity -> {
            final var transactionDto = new TransactionDto();
            transactionDto.setFrom(entity.getFrom());
            transactionDto.setTo(entity.getTo());
            transactionDto.setAmount(entity.getAmount());
            return transactionDto;
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(path = "transactions/{id}")
    public ResponseEntity<Void> revertTransaction(@PathVariable("id") UUID id) {
        transactionService.revertTransaction(id);
        return ResponseEntity.noContent().build();
    }
}
